package com.jungleegames.audience

import android.app.Application
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.annotation.NonNull
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jungleegames.audience.helper.MethodResultWrapper
import com.jungleegames.audience_sdk.AudienceSocket

import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.EventChannel
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry.Registrar
import io.reactivex.disposables.Disposable
import org.json.JSONArray
import org.json.JSONObject

/** AudiencePlugin */
class AudiencePlugin: FlutterPlugin, MethodCallHandler, EventChannel.StreamHandler {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private lateinit var context: Context
    private lateinit var dataChannel: MethodChannel
    private lateinit var eventChannel: EventChannel
    private var eventSink: EventChannel.EventSink? = null
    private var resultWrapper: MethodResultWrapper? = null
    private var _responseDisposable: Disposable? = null

  override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
      context = flutterPluginBinding.applicationContext
      dataChannel = MethodChannel(flutterPluginBinding.binaryMessenger, "com.jungleegames.pods/audience/data")
      eventChannel = EventChannel(flutterPluginBinding.binaryMessenger, "com.jungleegames.pods/audience/event")
      dataChannel.setMethodCallHandler(this)
      eventChannel.setStreamHandler(this)


  }

  override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
      resultWrapper = MethodResultWrapper(result)
      when (call.method) {
          "init" -> {
              initAudienceConnection(call.arguments())
              resultWrapper?.success(null)
          }
          "getDataList" -> {
              requestAudienceNetworkList()
              //The data is returned when result is received from audience-socket!
          }
          "acknowledge" -> {
              deleteAudienceNetworkData(call.arguments())
              resultWrapper?.success(null)
          }
          "disconnect" -> {
              closeAudienceNetworkConnection()
              resultWrapper?.success(null)
          }
          "getPlatformVersion" -> {

              result.success("Android ${android.os.Build.VERSION.RELEASE}")
          }          }

//    if (call.method == "getPlatformVersion") {
//
//    } else {
//      result.notImplemented()
//    }
  }

  override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
    dataChannel.setMethodCallHandler(null)
    eventChannel.setStreamHandler(null)
  }

    override fun onListen(arguments: Any?, events: EventChannel.EventSink?) {
        TODO("Not yet implemented")
    }

    override fun onCancel(arguments: Any?) {
        TODO("Not yet implemented")
    }


    /****************************** Utility Methods *******************************************/
    private fun initAudienceConnection(params: Map<String, Any>) {
        AudienceSocket.setAudienceUrl(params["url"] as String)
        AudienceSocket.setLogLevel(AudienceSocket.LogLevel.DEBUG)
        AudienceSocket.initializeConnection(
            context.applicationContext as Application,
            "jwr",
            params["userId"] as String,
            params["token"] as String
        )
        Log.v("AUDIENCE SDK", "url: ${params["url"]}, userId: ${params["userId"]}, token: ${params["token"]}");
        _responseDisposable = AudienceSocket.observeAudienceResponse()
            .subscribe { res: JSONObject ->
                when (res.getString("action")) {
                    "sm-inapp-list" -> {
                        sendAudienceDataList(res.getJSONArray("data"))
                    }
                    "sm-inapp" -> {
                        sendAudienceEvent(res.getJSONObject("data"))
                    }
                    "sm-inapp-delete" -> {
                        //Separate handling
                    }
                    else -> {
                    }
                }
            }
    }

    private fun closeAudienceNetworkConnection() {
        AudienceSocket.closeConnection()
        _responseDisposable?.dispose()
    }

    private fun deleteAudienceNetworkData(params: Map<String, Any>) {
        AudienceSocket.requestInAppDelete(params["trackingId"] as String, true)
    }

    private fun requestAudienceNetworkList() {
        AudienceSocket.requestInAppList()
    }

    private fun sendAudienceEvent(jsonObj: JSONObject?) {
        val gson: Gson = GsonBuilder().disableHtmlEscaping().create()
        Handler(Looper.getMainLooper()).postDelayed({
            eventSink?.success(gson.toJson(jsonObj))
        }, 20)
    }

    private fun sendAudienceDataList(jsonArray: JSONArray?) {
        val gson: Gson = GsonBuilder().disableHtmlEscaping().create()
        Handler(Looper.getMainLooper()).postDelayed({
            resultWrapper?.success(gson.toJson(jsonArray))
        }, 20)
    }
}
