package com.jungleegames.audience.helper

import android.os.Handler
import android.os.Looper
import android.util.Log
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.Result
import java.lang.Exception

class MethodResultWrapper constructor(result: Result?)  : Result  {
    private var methodResult: Result? = result
    private var handler: Handler? = Handler(Looper.getMainLooper())

    override fun success(result: Any?) {
        handler!!.post {
            try {
                Log.d("RESULT", result.toString())
            } catch (ignored: Exception) {
            }
            try {
                methodResult!!.success(result)
            } catch (ignored: Exception) {
            }
        }
    }

    override fun error(errorCode: String?, errorMessage: String?, errorDetails: Any?) {
        handler!!.post {
            try {
                methodResult!!.error(errorCode, errorMessage, errorDetails)
            } catch (ignored: Exception) {
            }
        }
    }

    override fun notImplemented() {
        handler!!.post {
            try {
                methodResult!!.notImplemented()
            } catch (ignored: Exception) {
            }
        }
    }


}