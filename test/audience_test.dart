import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:audience/audience_sdk_manager.dart';

void main() {
  const MethodChannel channel = MethodChannel('audience');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await AudienceSDKManager.platformVersion, '42');
  });
}
