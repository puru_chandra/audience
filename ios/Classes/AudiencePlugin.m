#import "AudiencePlugin.h"
#if __has_include(<audience/audience-Swift.h>)
#import <audience/audience-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "audience-Swift.h"
#endif

@implementation AudiencePlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftAudiencePlugin registerWithRegistrar:registrar];
}
@end
