class AudienceResponse {
  List<Values> values;

  AudienceResponse({
      this.values});

  AudienceResponse.fromJson(dynamic json) {
    if (json['values'] != null) {
      values = [];
      json['values'].forEach((v) {
        values.add(Values.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (values != null) {
      map['values'] = values.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

class Values {
  AudienceValue nameValuePairs;

  Values({
      this.nameValuePairs});

  Values.fromJson(dynamic json) {
    nameValuePairs = json['nameValuePairs'] != null ? AudienceValue.fromJson(json['nameValuePairs']) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (nameValuePairs != null) {
      map['nameValuePairs'] = nameValuePairs.toJson();
    }
    return map;
  }

}

class AudienceValue {
  int userID;
  String trackingId;
  String commName;
  String deepLink;
  int rank;
  int endTime;
  String content;
  int deliveryChannel;
  String customData;
  String id;
  int ts;

  AudienceValue({
      this.userID, 
      this.trackingId,
      this.commName, 
      this.deepLink, 
      this.rank, 
      this.endTime, 
      this.content, 
      this.deliveryChannel, 
      this.customData, 
      this.id, 
      this.ts});

  AudienceValue.fromJson(dynamic json) {
    userID = json['UserID'];
    trackingId = json['TrackingID'];
    commName = json['CommName'];
    deepLink = json['DeepLink'];
    rank = json['Rank'];
    endTime = json['EndTime'];
    content = json['Content'];
    deliveryChannel = json['DeliveryChannel'];
    customData = json['CustomData'];
    id = json['ID'];
    ts = json['TS'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['UserID'] = userID;
    map['TrackingID'] = trackingId;
    map['CommName'] = commName;
    map['DeepLink'] = deepLink;
    map['Rank'] = rank;
    map['EndTime'] = endTime;
    map['Content'] = content;
    map['DeliveryChannel'] = deliveryChannel;
    map['CustomData'] = customData;
    map['ID'] = id;
    map['TS'] = ts;
    return map;
  }

}