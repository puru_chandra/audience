import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'model/audience_response.dart';

class AudienceSDKManager {
  static final AudienceSDKManager _instance = AudienceSDKManager._internal();

  AudienceSDKManager._internal();

  factory AudienceSDKManager() => _instance;

  static const MethodChannel _methodChannel =
      const MethodChannel('com.jungleegames.pods/audience/data');
  static const EventChannel _eventChannel =
      const EventChannel('com.jungleegames.pods/audience/event');

  Future<bool> init({
    @required String audienceUrl,
    @required String token,
    @required String userId,
  }) async {
    return await _methodChannel.invokeMethod<bool>(
      "init",
      {"token": token, "userId": userId, "url": audienceUrl},
    );
  }

  Stream<AudienceValue> getAudienceEventStream() {
    return _eventChannel.receiveBroadcastStream().map<AudienceValue>((event) {
      return Values.fromJson(jsonDecode(event)).nameValuePairs;
    });
  }

  Future<List<AudienceValue>> fetchCurrentAudienceDataList() async {
    String json = await _methodChannel.invokeMethod("getDataList");
    List<AudienceValue> audienceValueList = [];
    AudienceResponse audienceResponse =
        AudienceResponse.fromJson(jsonDecode(json));
    audienceResponse?.values?.forEach((v) {
      audienceValueList.add(v.nameValuePairs);
    });
    return audienceValueList;
  }

  Future<void> sendAcknowledgement({String id}) async {
    return await _methodChannel.invokeMethod("acknowledge", {"trackingId": id});
  }

  Future<void> disconnect() async {
    await _methodChannel.invokeMethod("disconnect");
  }

  //platform version method
  static Future<String> get platformVersion async {
    final String version =
        await _methodChannel.invokeMethod('getPlatformVersion');
    return version;
  }
}
